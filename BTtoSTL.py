from __future__ import unicode_literals
from PyQt5 import QtCore, QtGui, QtWidgets
import os
from vtk.util import numpy_support
import vtk
from PyQt5.Qt import *
from PyQt5.QtWidgets import QApplication, QWidget,QAction
from vtk import *
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
import sys

stlDosyasi=None

try:
        _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
        def _fromUtf8(s):
                return s
try:
        _encoding = QtGui.QApplication.UnicodeUTF8
        def _translate(context, text, disambig):
                return QtGui.QApplication.translate(context, text, disambig,
_encoding)
except AttributeError:
        def _translate(context, text, disambig):
                return QtGui.QApplication.translate(context, text, disambig)


class pencere(QMainWindow):
    def __init__(self,parent=None):
        super(pencere,self).__init__()

        self.setGeometry(50, 75, 1150, 900)
        self.setWindowTitle('BT to STL ')

        self.textbox = QLineEdit("stl adı",self)
        self.textbox.move(20, 45)
        self.textbox.resize(250, 30)


        self.statusWidget=QtWidgets.QListWidget(self)
        self.statusWidget.setObjectName('ListView')
        self.statusWidget.setAlternatingRowColors(True)
        self.statusWidget.resize(250,230)
        self.statusWidget.move(20,95)
        self.statusWidget.setWordWrap(True)

        self.listWidget=QtWidgets.QListWidget(self)
        self.listWidget.setObjectName('ListView')
        self.listWidget.setAlternatingRowColors(True)
        self.listWidget.resize(250,500)
        self.listWidget.move(20,350)



        menubar = self.menuBar()
        dosya = menubar.addMenu("Dosya")
        #islemler = menubar.addMenu("İşlemler")
        #yardim = menubar.addMenu("Yardım")
        klasor_ac = QAction("STL Çıkar (DICOM Klasörü Aç)", self)
        klasor_ac.setShortcut("Ctrl+O")
        stl_ac = QAction("STL Aç", self)
        stl_ac.setShortcut("Ctrl+S")
        dosya_cikis = QAction("Çıkış", self)
        dosya_cikis.setShortcut("Ctrl+Q")

        dosya.addAction(klasor_ac)
        dosya.addAction(stl_ac)
        dosya.addAction(dosya_cikis)


        stl_cikar = QAction("STL Çıkar", self)
        stlKlasorAc = QAction("STL Klasör Aç", self)
        #islemler.addAction(stl_cikar)
        #islemler.addAction(stlKlasorAc)

        dosya_cikis.triggered.connect(self.uygulamaKapat)
        klasor_ac.triggered.connect(self.dosyaac)
        stl_ac.triggered.connect(self.stlAc)

        #stl_cikar.triggered.connect(self.stlCikar)


        #self.init_ui()

    def uygulamaKapat(self):
        icon = None
        if(QMessageBox.question(icon,"Çıkış yapılıyor!","Çıkmak istediginize emin misiniz?",QMessageBox.Yes | QMessageBox.No)==QMessageBox.Yes):
            app.quit()

    def stlAc(self):
        dosya_git = QtWidgets.QFileDialog.getOpenFileName(self, 'STL için gözat', './',filter="*.stl")

        if dosya_git:
            #print(dosya_git[0])
            #print(type(dosya_git))
            global stlDosyasi
            stlDosyasi = dosya_git[0]
            self.init_ui()
            #vtkobjects.importstl(self,stlDosyasi)
            Pencere.vtkWidget.iren.Initialize()
            self.statusWidget.addItem("STL yüklendi.")


    def dosyaac(self):
        dosya_git = QtWidgets.QFileDialog.getExistingDirectory(self, 'DICOM görüntüleri için gözat', '')
        if dosya_git:
            dosya_List = os.listdir(str(dosya_git))

            if dosya_List:
                isim=self.textbox.text()
                if isim!="stl adı":
                    self.listWidget.clear()
                    for eachFile in dosya_List:
                        self.listWidget.addItem(eachFile)
                    self.stlYazdir(dosya_git,isim)
                    self.statusWidget.addItem(isim+" isimli stl dosyası çalışma dizinine kaydedildi.")
                else:
                    QMessageBox.warning(None, "Dikkat",
                                        "stl adını giriniz!!")


    def init_ui(self):

        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.setCentralWidget(self.centralwidget)

        self.grid = QtWidgets.QGridLayout(self.centralwidget)
        self.grid = QtWidgets.QGridLayout(self.listWidget)
        self.grid = QtWidgets.QGridLayout(self.statusWidget)
        spacerItem = QtWidgets.QSpacerItem(20, 50, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.grid.addItem(spacerItem)

        self.vtkWidget = vtkobjects(self.centralwidget)
        self.vtkWidget.resize(800, 800) # Widget boyutu

    def stlYazdir(self,PathDicom, stlAdi):

        reader = vtk.vtkDICOMImageReader()
        reader.SetDirectoryName(PathDicom)
        reader.Update()

        _extent = reader.GetDataExtent()

        ConstPixelDims = [_extent[1] - _extent[0] + 1, _extent[3] - _extent[2] + 1, _extent[5] - _extent[4] + 1]
        #print(ConstPixelDims,"burada1")

        threshold = vtk.vtkImageThreshold()
        threshold.SetInputConnection(reader.GetOutputPort())
        threshold.ThresholdByLower(400)  # remove all soft tissue
        threshold.ReplaceInOn()
        threshold.SetInValue(0)  # set all values below 400 to 0
        threshold.ReplaceOutOn()
        threshold.SetOutValue(1)  # set all values above 400 to 1
        threshold.Update()
        #print( "burada2")
        dmc = vtk.vtkDiscreteMarchingCubes()
        dmc.SetInputConnection(threshold.GetOutputPort())
        dmc.GenerateValues(1, 1, 1)
        dmc.Update()

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(dmc.GetOutputPort())

        writer = vtk.vtkSTLWriter()
        writer.SetInputConnection(dmc.GetOutputPort())
        writer.SetFileTypeToBinary()
        writer.SetFileName(stlAdi + ".stl")
        writer.Write()

class vtkobjects(QVTKRenderWindowInteractor):
    def __init__(self, centralwidget):
        super(vtkobjects, self).__init__(centralwidget)
        self.ren = vtk.vtkRenderer()
        self.move(300,20)     # Konumlandırıcı Widget
        self.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.GetRenderWindow().GetInteractor()
        self.ren.SetBackground(.3, .4, .5)
        #print(stlDosyasi,"###")
        self.importstl(stlDosyasi)



    #####  Import a STL file #####
    def importstl(self,dosyaAdi):


        reader = vtk.vtkSTLReader()
        reader.SetFileName(dosyaAdi)

        cm1 = vtk.vtkPolyDataMapper()

        if vtk.VTK_MAJOR_VERSION <= 5:
            cm1.SetInput(reader.GetOutput())

        else:
            cm1.SetInputConnection(reader.GetOutputPort())

        self.panel = vtk.vtkActor()
        self.panel.SetMapper(cm1)
        self.panel.SetPosition(0, 0, 0)

        self.panel.GetProperty().SetDiffuseColor(1, 1, 1)  #Obje Rengi
        self.panel.GetProperty().SetOpacity(.90)            #Opak Ayarı
        self.panel.GetProperty().SetSpecular(.20)              #yansıtıcılık

        ##### STL rotation
        self.paneltransform = vtk.vtkTransform()
        self.panel.SetUserTransform(self.paneltransform)
        self.paneltransform.RotateX(0)
        self.paneltransform.RotateY(0)


        ##### STL scaling (ölçeklendirme)#####
        prop = vtk.vtkProp3D
        self.panel.SetScale(1)

        ##### render STL
        self.ren.AddActor(self.panel)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Pencere = pencere()
    Pencere.show()
    #Pencere.vtkWidget.iren.Initialize()
    sys.exit(app.exec_())
